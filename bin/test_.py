import os
import filecmp
import pytest

# https://docs.pytest.org/en/latest/how-to/fixtures.html#safe-teardowns
@pytest.fixture
def tear_down_diskm():
  yield None
  os.remove('atlas_guyane_laurales.k7.dis')

@pytest.fixture
def tear_down_disedi():
  yield None
  os.remove('atlas_guyane_laurales.sw.dis')
  os.remove('atlas_guyane_laurales.bin')


def test_disedi(tear_down_disedi):
  cmd = 'disedi\
      -q ../datasets/atlas_guyane_laurales.fas\
      -o atlas_guyane_laurales'
  os.system(cmd)
  assert filecmp.cmp('atlas_guyane_laurales.sw.dis','data4tests/atlas_guyane_laurales.sw.dis')

def test_diskm(tear_down_diskm):
  cmd = 'diskm\
      -q ../datasets/atlas_guyane_laurales.fas\
      -o atlas_guyane_laurales\
      -k 7 -f dis'
  os.system(cmd)
  assert filecmp.cmp('atlas_guyane_laurales.k7.dis','data4tests/atlas_guyane_laurales.k7.dis')

