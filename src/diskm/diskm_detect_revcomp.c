/****************************************
 * File:   diskm
 * Author: jmf
 *
 * Created  mercredi 26 juillet 2023 15:00
 *  Version 0.0.1
 *  Copyright INRAE 2023

This file is part of disseq and mpidisseq.
disseq and mpidisseq are free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 */


/*
 * Ce binaire est lancé par diskm_jelly.py
 *
 * Input: k et nk (nombre de kmers réalisés):
 *
 *        Les fichiers générés par diskm_jelly.py:
 *          seqids: fichiers des seqids
 *          basename.kX.jfm (binaire), basename.kX.kmers (texte)
 *          fichiers *.jfas et *.jf
 *
 *  Affiche les résultats sous forme d'un fichier *.dis
 *  (matrice carrée avec nom de lignes et colonnes)
 *    
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
// #include <time.h>
// #include <sys/stat.h>
// #include <dirent.h>
// #include <hdf5.h>

//#define DEBUG
#define MIN(x,y)     ((x)<=(y) ? (x) : (y))

#define timing(a) start=clock(); a; diff = clock() - start; msec = diff * 1000 / CLOCKS_PER_SEC; printf("msecs: %d\n",msec);

int read_jf(char * jfile, unsigned char k_oct, unsigned int nk, unsigned int count[])
{
  FILE *fp_i;
  char file[127];
  strcpy(file,jfile);
  strncat(file,".jf",4);
  fp_i = fopen(file, "rb");
  if (fp_i == NULL)
  {
    perror("fopen file");
    printf("%s\n",file);
    getchar();
    return EXIT_FAILURE;
  }
  char offset_c[10]; offset_c[9] = '\0';
  unsigned int offset;
  size_t ret;
  ret = fread(offset_c, sizeof(char), 9, fp_i);
  if (ret != 9)
  {
    perror("fread offset_c");
    return EXIT_FAILURE;
  }

  offset = atol(offset_c);
  char header[offset]; header[offset] = '\0';
  ret = fread(header,sizeof(char), offset, fp_i);
  if (ret != offset)
  {
    printf("ret %zu\n", ret);
    perror("fread offset");
    return EXIT_FAILURE;
  }
  unsigned int kmer; //[16];
  unsigned int c = 0;
  unsigned int n_count = 0;

  for (unsigned int n=0; n<nk; ++n)
  {
    ret = fread(&kmer,  sizeof(char), k_oct, fp_i);
    ret = fread(&c, sizeof(unsigned int), 1, fp_i);
    if (ret == 0) break;
    count[n] = c;
  }
  fclose(fp_i);
}

void *chomp(char *str)
{
  if(str[strlen(str)-1] == 10) str[strlen(str)-1] = 0;
}
 
int main(int argc, char** argv)
{
  printf("NOT RUNNING\n");
  exit(1);
  unsigned char k  = atoi(argv[1]);
  unsigned int  nk = atoi(argv[2]);
  //printf("NK %d\n",nk);
  unsigned char k_oct;
  while(1)
  {
    if      (k <= 4)   { k_oct = 1; break; }
    else if (k <= 8)   { k_oct = 2; break; }
    else if (k <= 16)  { k_oct = 3; break; }
    else if (k <= 32)  { k_oct = 4; break; }
    else if (k <= 64)  { k_oct = 5; break; }
    else if (k <= 128) { k_oct = 6; break; }
    else { printf("k=%hu !! Can't use k > 128",k); exit(0); }
  }

  FILE *seql;
  char seqid[127];
  if ((seql = fopen("seqids","r")) == NULL)
  {
    perror("fopen seqids");
    return EXIT_FAILURE;
  }
  //Nb de seqids == nb de lignes
  unsigned int n_seq = 0;
  while (fgets(seqid,127,seql)) ++n_seq;
  rewind(seql);

  char jflist[n_seq][127];
  unsigned int n = 0;
  while (fgets(seqid,127,seql))
  {
    chomp(seqid);
    strcpy(jflist[n],seqid);
    ++n;
  }
  unsigned int count_i[nk];
  unsigned int count_i_rev[nk];
  unsigned int count_j[nk];
  unsigned int d =  0;
  unsigned int d_rev = 0;
  //unsigned int * distances = (unsigned int *) calloc(n_seq * n_seq, sizeof(unsigned int));
/*
for(unsigned int i=0; i<n_seq; ++i)
{
  printf("%s\n",jflist[i]);
}
*/
  read_jf(jflist[0], k_oct, nk, count_i);
  char revcomp[132];
  sprintf(revcomp,"%s_rev",jflist[0]);
  read_jf(revcomp, k_oct, nk, count_i_rev);

  //distances[i * n_seq + i] = 0;
  for (unsigned int j=0; j<n_seq; ++j)
  {
    read_jf(jflist[j], k_oct, nk, count_j);
    d = 0;
    d_rev = 0;
    for (unsigned int k=0; k<nk; ++k)
    {
      d     += abs(count_i[k]     - count_j[k]);
      d_rev += abs(count_i_rev[k] - count_j[k]);
    }
//if (d_rev < d)
//   {
      printf("D(0,%d): %s x %s %d %d\n",j, jflist[0], jflist[j], d, d_rev);
//printf("%d\n",j);
//    }
    //distances[i * n_seq + j] = d;
    //distances[j * n_seq + i] = d;
  }
  /*
  for (unsigned int col=0; col<n_seq; ++col)
  {
    printf("\t%d",distances[i * n_seq + col]);
  }
  printf("\n");
  }
  */
}

  /*
  for (int i=0; i<n; ++i)
  {
    printf("%s\n",seqlist[i]);
    fread(offset_c, sizeof(char), 9, fp_i);
    unsigned int offset = atol(offset_c);// + 9;
    printf("%d\n", offset);
  }
  */
 /* 
  unsigned char k     = 3;
  unsigned char k_oct = 1; 
  char jFile[127] = "10A10C_k3.jf";//"10A_k3.jf";"10A10C_k5.jf";
  FILE *fp_j      = fopen(jFile, "rb");
  if (!fp_j) {
    perror("fopen");
    return EXIT_FAILURE;
  }
  size_t ret;
  char offset_c[9]; offset_c[9] = '\0';
  fread(offset_c, sizeof(char), 9, fp_j);
  unsigned int offset = atol(offset_c);// + 9;
  printf("%d\n", offset);
  char header[offset]; header[offset] = '\0';
  fread(header,sizeof(char), offset, fp_j);
  printf("%s\n", header);
  printf("offset %u\n", offset);
  unsigned int kmer; //[16];
  unsigned int count;
  while(1)
  {
    fread(&kmer,  sizeof(char),         k_oct, fp_j);
    fread(&count, sizeof(unsigned int),     1, fp_j);
    if (feof(fp_j) != 0) break;
    printf("kmer: %hhu count %u\n",kmer, count);
  }

}*/


// int main(int argc, char** argv)
// {
// 
//   //printf("NOT RUNNING !!!!!"); exit(0);
//   char queryFile[127];
//   char refFile[127];
//   char revFile[127];
//   char masterFile[127];
//   char outFile[127];
// 
//   sprintf(queryFile, "%s.jfkm", argv[1]);
//   sprintf(refFile, "%s", queryFile);
//   sprintf(revFile, "%s_rev.jfkm", argv[1]);
//   sprintf(masterFile, "%s.master.jfkm", argv[1]);
//   sprintf(outFile, "%s.dkm", argv[1]);
// 
//   FILE *fp_query     = fopen(queryFile, "r");
//   FILE *fp_rev_query = fopen(revFile, "r");
//   FILE *fp_ref       = fopen(refFile,   "r");
//   FILE *out          = fopen(outFile, "w");
//   
//   if (! fp_ref)
//   {
//     fprintf(stderr,"%s: ",argv[1]);
//     perror("");
//     return EXIT_FAILURE;
//   }
// /* Test histogramme
//   unsigned int n_histo;
//   n_histo = test_histo(fp_ref);
//   printf("n_histo %d\n",n_histo);
//   exit(0);
// */
//   
//   unsigned int n_ref, n_query, k_ref, k_query, n_rev_query, k_rev_query;
//   fread(&n_query,sizeof(unsigned int),1,fp_query);
//   fread(&n_rev_query,sizeof(unsigned int),1,fp_rev_query);
//   fread(&n_ref,sizeof(unsigned int),1,fp_ref);
// 
//   fread(&k_query,sizeof(unsigned int),1,fp_query);
//   fread(&k_rev_query,sizeof(unsigned int),1,fp_rev_query);
//   fread(&k_ref,sizeof(unsigned int),1,fp_ref);
//   if(k_query != k_ref)
//   {
//     printf("!!!!! k_query %d != k_ref %d !!!!!\n",k_query,k_ref);
//     exit(1);
//   }
//   printf("n_query %d n_ref %d\n",n_query, n_ref);
//   printf("n_rev_query %d \n",n_rev_query);
//   //master (les kmers réalisés)
//   struct stat filestat;
//   stat(masterFile, &filestat);
//   unsigned int n_all_k = filestat.st_size / k_ref;
//   unsigned int * h0_pos;
//   unsigned int * h0_count;
//   unsigned int * h0_rev_pos;
//   unsigned int * h0_rev_count;
//   unsigned int * h1_pos;
//   unsigned int * h1_count;
//   
//   h0_pos       = calloc(n_all_k, sizeof( unsigned int));
//   h0_count     = calloc(n_all_k, sizeof( unsigned int));
//   h0_rev_pos   = calloc(n_all_k, sizeof( unsigned int));
//   h0_rev_count = calloc(n_all_k, sizeof( unsigned int));
//   h1_pos       = calloc(n_all_k, sizeof( unsigned int));
//   h1_count     = calloc(n_all_k, sizeof( unsigned int));
//   
//   unsigned int d, d_rev;
//   unsigned int h0_l = 0, h0_rev_l = 0, h1_l = 0;
//   unsigned long h1_fpos = 0;
//   int nq = 0, nr = 0;
// 
//   // 1 vector d(i,j) ==> distances[i * n_ref + j]
//   unsigned int * distances = (unsigned int *) calloc(n_query * n_ref, sizeof(unsigned int));
//   if (strcmp(queryFile, refFile) == 0) // 2 fois le même fichier d'entrée
//   {
// 
//      // Calcule diag sup, écrit tout
//       for (unsigned int i=0; i<n_query;++i)
//       {
//         h0_l     = get_histo(fp_query,    h0_pos,    h0_count);
//         h0_rev_l = get_histo(fp_rev_query,h0_rev_pos,h0_rev_count);
//         h1_l     = get_histo(fp_ref, h1_pos, h1_count);
//         if(h0_l == -1) break;
//         h1_fpos = ftell(fp_ref);
//         for (unsigned int j=i+1; j<n_ref; ++j)
//         {
//           h1_l = get_histo(fp_ref,h1_pos,h1_count);
//           if(h1_l == -1) break;   
//           d     = distance(h0_pos, h0_count, h1_pos, h1_count, h0_l, h1_l);
//           if(h0_rev_l != -1) //pas de revcomp
//           {
//             d_rev =  distance(h0_rev_pos, h0_rev_count, h1_pos, h1_count, h0_rev_l, h1_l);
//             d = MIN(d, d_rev);
//           }
//           distances[i * n_ref + j] = d;
//           distances[j * n_ref + i] = d;
//         }
//         fseek(fp_ref,h1_fpos,SEEK_SET);
//       }
//   }
//   else  
//   {
//     // ALL vs ALL NON IMPLEMENTÉ
//     for (unsigned int i=0; i<n_query;++i)
//       {
//         h0_l = get_histo(fp_query,h0_pos,h0_count);
//         if(h0_l == -1) break;
//         for (unsigned int j=0; j<n_ref;++j)
//         {
//           h1_l = get_histo(fp_ref,h1_pos,h1_count);
//           if(h1_l == -1) break;   
//           d = distance(h0_pos, h0_count, h1_pos, h1_count, h0_l, h1_l);
//           distances[i * n_ref + j] = d;
//           distances[j * n_ref + i] = d;
//         }
//         fseek(fp_ref,0,SEEK_SET);
//       }   
//   }
//   fwrite(&n_query, sizeof(unsigned int), 1,                out);
//   fwrite(&n_ref,   sizeof(unsigned int), 1,                out);
//   fwrite(distances,sizeof(unsigned int), n_query *  n_ref, out);
// 
// /*
//   if((argc == 3 && strcmp(argv[2], "dis") == 0))
//   {
//     printf("Yeaah ascii\n");
//   }
// */
// /*support hdf5 eliminé
//   if (argc == 6)
//   {
//     writeHdf5(n_query, n_ref, n_ref, 0, distances, argv[5]);
//   }
//   */
//   fclose(fp_query);
//   fclose(fp_ref);
//   free(distances);
//   fclose(out);
//   free(h0_pos);
//   free(h0_count);
//   free(h1_pos);
//   free(h1_count);
// }
