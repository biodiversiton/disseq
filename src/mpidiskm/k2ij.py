#!/usr/bin/env python3

import sys
import math

n = int(sys.argv[1])

b = 2 * n - 1
for ip in range(1, int(n*(n-1)/2)):
  d = b**2 - 8 * ip
  i = math.ceil((b - math.sqrt(d))/2 - 1)
  j = ip + i - n*(n-1)/2 + (n-i)*((n-i)-1)/2
  print(f'pour {n=} et {ip=} ==> {i=} {j=}')

