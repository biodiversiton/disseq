/* 
 * File:   com_int_param.h
 * Author: Philippe
 *
 * Created on 23 août 2012, 15:08
 * Copyright INRAE 2022

This file is part of disseq.
disseq is a free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 */

#ifndef COM_INT_PARAM_H
#define	COM_INT_PARAM_H

int nb_ascii = 127 + 1;
int L_A = 'A';
int L_a = 'a';
int L_C = 'C';
int L_c = 'c';
int L_G = 'G';
int L_g = 'g';
int L_N = 'N';
int L_n = 'n';
int L_T = 'T';
int L_t = 't';
int tiret = '-';
int nb_nucleotides = 11;

//int L_A = 'A';
//int L_C = 'C';


int L_D = 'D';
int L_E = 'E';
int L_F = 'F';
//int L_G = 'G';
int L_H = 'H';
int L_I = 'I';
int L_K = 'K';
int L_L = 'L';
int L_M = 'M';
//int L_N = 'N';
int L_P = 'P';
int L_Q = 'Q';
int L_R = 'R';
int L_S = 'S';
//int L_T = 'T';
int L_V = 'V';
int L_W = 'W';
int L_Y = 'Y';
int nb_aa = 21;

#endif	/* COM_INT_PARAM_H */

