/*
 * File:   com_param.h
 * Author: Philippe
 *
 * Created on 21 août 2012, 12:00
 * 
 *  Copyright INRAE 2022

This file is part of disseq.
disseq is a free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 */

#ifndef COM_PARAM_H
#define	COM_PARAM_H

//---paramètres internes au programme liès aux dimensionnement de variables et tableaux (pas de modification requises)---
#define MAXFILENAME     255     // Filename max length
#define MAXLINE         4000    // Line max length
#define MAXIDLEN        255	// Identifier max length
#define MAXSEQLEN       4000    // Sequence max length
#define DELTA_MAJ_MIN   32      // Upper vs lower difference code ASCII
#define ASCII_ETOILE    42      // valeur ASCII de l'étoile

#define MAX2(x,y)     ((x)<(y) ? (y) : (x))
#define MAX3(x,y,z)   (MAX2(x,y)<(z) ? (z) : MAX2(x,y))
#define MIN2(x,y)     ((x)<(y) ? (x) : (y))
#define MIN3(x,y,z)   (MIN2(x,y)<(z) ? MIN2(x,y) : (z))

#define foreach( intpvar, intary ) int* intpvar; for( intpvar=intary; intpvar < (intary + (sizeof(intary)/sizeof(intary[0]))) ; intpvar++)

//--- Définition des structures du programme (non modifiables)
struct Oneseq{
    char    *id;   //identifier
    char    *dna;  //DNA sequence
    char    *rdna; //DNA sequence reverse complement
    int      len;   //length
};

struct Paire {
    int    i;
    int    j;
};

#endif	/* COM_PARAM_H */
