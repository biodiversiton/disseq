/****************************************
 * File:   mpidisseq/main.c
 * Author: jmf
 *
 * Created vendredi 27 juin 2014 14:16
 *  Version 0.0.1
 *  IDRIS (Sylvie Thérond) 8 avril 2015
 *  Version 1.0.0
 ****************************************
 * 16/04/2015
 * File:   mpidisseq/mpidisseqAll_full.c
 *
 * 27/05/2016 mpidisseq-1.1.9
 * 12/01/2018:
 * File:  src/mpidisseqAll_full.c
 * header in the binary file
 * 3 ints: number of line, number of columns, datatype (float32=4)
 *
 * vendredi 18 février 2022 13:56
 * File: mpidisseq/mpidisseq.c
 * hdf5 support: Florent Pruvost (INRIA)
 *
 * vendredi 26 juillet 2024 16:10
 * became mpidisedi
 *  output  hdf5 or binary file of floats
 *
 *  Copyright INRAE 2022

This file is part of disseq.
disseq is a free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>
#include "com_param.h"
#include "com_func.h"
#include "param.h" //PARAM_H
#include "func.h" //FUNC_H

int main(int argc, char** argv)
{
  char usage[] = "usage:\n mpiexec -np number_of_procs mpidisedi -query QUERYFILE -out OUTFILE [-ref REFFILE] [-algo {sw(default), nw}] [-format {full(default), sparse, h5}]\n";
  if(argc < 3)
  {
    printf("%s", usage);
    exit(0);
  }

  int rang, nProc,err = 1;
  MPI_Init(&argc, &argv);
  MPI_Comm comm;
  MPI_Status status;
  comm = MPI_COMM_WORLD;
  MPI_Comm_rank(comm, &rang);
  MPI_Comm_size(comm, &nProc);
  FILE *fh_query, *fh_ref;
  char algo[3]; algo[0] = '\0';     strcat(algo, "sw");
  char refFile[MAXFILENAME];        refFile[0]       = '\0';
  char queryFile[MAXFILENAME];      queryFile[0]     = '\0';
  char outFile[MAXFILENAME];   outFile[0]  = '\0';
  int root     = 0; 
  float dismax  = 0;
  time_t begin = time( NULL );
  time_t end;

  load_arguments(argc, argv, queryFile, refFile, outFile, algo, &dismax);

  if( (strcmp(algo, "sw") != 0) &&  (strcmp(algo, "nw") != 0) )
  {
    fprintf(stderr,"Algo %s unknown\n", algo);
    fprintf(stderr,"%s",usage);
    MPI_Abort(comm,err);
  }
  
  int maxseqlen = 0;//MAXSEQLEN;
  int maxidlen  = 0;//MAXIDLEN;

  if (( fh_query = fopen( queryFile , "r" ) ) == NULL)
  {
    fprintf(stderr,"%s",usage);
    fprintf(stderr, "Can't open queryFile %s\n",queryFile);
    MPI_Abort(comm,err);
  }

  if (strcmp(refFile,"") == 0)
    strcat(refFile,queryFile);
  
  if (( fh_ref = fopen( refFile , "r" ) ) == NULL)
  {
    fprintf(stderr, "Can't open refFile %s\n",refFile);
    fprintf(stderr,"%s",usage);
    MPI_Abort(comm,err);
  }

  int nQuery;
  int nRefseq;
  long int *seqPos  = NULL; //long long int
  get_fasta_infos_with_pos(fh_query,&nQuery,&maxseqlen,&maxidlen,&seqPos);
  int maxseqlenq  = maxseqlen;
  int maxidlenq   = maxidlen;
  get_fasta_infos(fh_ref,&nRefseq,&maxseqlen,&maxidlen);
  maxseqlen       = MAX2(maxseqlenq,maxseqlen);
  maxidlen        = MAX2(maxidlenq,maxidlen);

  //Share the work
  if (nQuery <  nProc)
  {
     fprintf(stderr, "Too few queries (%d) for too many procs (%d)\n",nQuery,nProc);
     fprintf(stderr,"%s",usage);
     MPI_Abort(comm,1);
  }

  int by_proc = nQuery/nProc;
  if (nQuery % nProc > rang) by_proc++;

  int suivant   = (rang + 1) % nProc;
  int precedent = (nProc + rang - 1) % nProc;

  int first_seq = 0;
  int last_seq  = by_proc - 1;

// Ring communication
  if (rang == 0)
  {
    MPI_Send(&last_seq,1,MPI_INT,suivant,1,comm);
    MPI_Recv(&first_seq,1,MPI_INT,precedent,1,comm,MPI_STATUS_IGNORE);
    first_seq = 0;
    last_seq  = by_proc - 1;
  }
  else
  {
    MPI_Recv(&first_seq,1,MPI_INT,precedent,1,comm,MPI_STATUS_IGNORE);
    last_seq = first_seq + by_proc;
    first_seq += 1;
    MPI_Send(&last_seq,1,MPI_INT,suivant,1,comm);
  }

  struct Oneseq *refseqlist = NULL;
  refseqlist = calloc((nRefseq+1),sizeof (struct Oneseq));
  for(int i=0; i < nRefseq; ++i)
  {
    refseqlist[i].id    = calloc((maxidlen +1 ), sizeof(char));
    refseqlist[i].dna   = calloc((maxseqlen +1), sizeof(char));
    refseqlist[i].rdna  = calloc((maxseqlen +1), sizeof(char));
  }

  read_all_ref_seq(refFile,refseqlist,maxseqlen,maxidlen,nRefseq,comm);

  int nCalc     = 0;
  struct Oneseq queryseq;
  queryseq.id   = calloc((maxidlen +1), sizeof(char) );
  queryseq.dna  = calloc((maxseqlen +1),sizeof(char) );//free
  queryseq.rdna = calloc((maxseqlen +1),sizeof(char) );// Not used
  float my_dist = 0;

  //creation of the SW matrix
  float **SW = NULL;
  SW = alloc_NW_matrix(maxseqlen+1); //from Commun_func

  ///creation of comp_Matrix
  float **comp_Matrix = NULL;
  comp_Matrix = build_comp_matrix();//from Commun_func

  /* Need to store the matrix data as a vector (row major) for hdf5 */
  float *matrix = (float *)malloc(by_proc*nRefseq*sizeof(float));

  float (*compute_distance)(struct Oneseq, struct Oneseq,  float **, float **);
  if(strcmp(algo,"sw") == 0)    compute_distance = sw_distance;
  if(strcmp(algo,"nw") == 0)    compute_distance = nw_distance;
  /*
  end = time( NULL);
  fprintf(stderr,"%d: Init in %lu sec\n",rang, (unsigned long) difftime( end, begin ));
  begin = time( NULL);
  */
  for (int p = first_seq; p <= last_seq; ++p)
  {
    fseek(fh_query,seqPos[p], SEEK_SET);
    next_seq(fh_query,&queryseq,maxseqlen,maxidlen);
    for (int r = 0; r < nRefseq; ++r)
    {
      my_dist = compute_distance(queryseq, refseqlist[r], SW,comp_Matrix);
      matrix[(p-first_seq)*nRefseq + r] = my_dist;
    }
    //diagonal = 0
     matrix[(p-first_seq)*nRefseq + p] = 0;
  }
  //CLOSE FILES
  fclose(fh_ref);
  /*
  end = time( NULL);
  fprintf(stderr,"%d: distances computed in  in %lu sec\n",rang, (unsigned long) difftime( end, begin ));
  begin = time( NULL);
  */


/*
  if (strcmp(format, "h5") == 0)
  { 
    char *seqidqueryh5[nQuery];
    char *seqidrefh5[nRefseq];
    char *wordqueryh5[nQuery];
    char *wordrefh5[nRefseq];
    if (rang == 0){
      // need a char* array for hdf5 write interface, here seq query (rows)
      for (int i=0; i < nQuery; ++i)
      {
        fseek(fh_query,seqPos[i], SEEK_SET);
        next_seq(fh_query,&queryseq,maxseqlen,maxidlen);
        seqidqueryh5[i] = (char *)malloc(MAXIDLEN*sizeof(char));
        strcpy(seqidqueryh5[i], queryseq.id);
        wordqueryh5[i] = (char *)malloc(MAXSEQLEN*sizeof(char));
        strcpy(wordqueryh5[i], queryseq.dna);
      }
      // need a char* array for hdf5 write interface, here seq ref (cols)
      for (int i=0; i<nRefseq; i++){
        seqidrefh5[i] = (char *)malloc(MAXIDLEN*sizeof(char));
        strcpy(seqidrefh5[i], refseqlist[i].id);
        wordrefh5[i] = (char *)malloc(MAXSEQLEN*sizeof(char));
        strcpy(wordrefh5[i], refseqlist[i].dna);
      }
    }
    fclose(fh_query);
    
    // save the matrix in parallel with hdf5
    if (strcmp(refFile,queryFile) == 0)
    {
      writeHdf5(nQuery, nRefseq, by_proc, first_seq, matrix, seqidqueryh5, NULL, wordqueryh5, NULL, outFile);
    }
    else
    {
      writeHdf5(nQuery, nRefseq, by_proc, first_seq, matrix, seqidqueryh5, seqidrefh5, wordqueryh5, wordrefh5, outFile);
    }
    // free the local matrix
    free(matrix);
    if (rang == 0){
      // free the sequence ids
      for (int i=0; i<nQuery; i++){
          free(seqidqueryh5[i]);
          free(wordqueryh5[i]);
      }
      for (int i=0; i<nRefseq; i++){
          free(seqidrefh5[i]);
          free(wordrefh5[i]);
      }
    }
  }
  else
  {
*/
  // save floats, python will reformat
  MPI_File fh_out;
  strcat(outFile,".bin");
  if (MPI_File_open(MPI_COMM_WORLD, outFile, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh_out) != MPI_SUCCESS)
    printf("%d: %s inaccessible\n",rang, outFile);

  MPI_File_write_ordered(fh_out, matrix, by_proc*nRefseq,  MPI_FLOAT, &status);
  MPI_File_close(&fh_out);
  
  /*
  end = time( NULL);
  fprintf(stderr,"%d: Saved in in %lu sec\n",rang, (unsigned long) difftime( end, begin ));
  */
  free(queryseq.id);
  free(queryseq.dna);
  free(queryseq.rdna);

  for (int i = 0; i < nRefseq; ++i )
  {
    free(refseqlist[i].id );
    free(refseqlist[i].dna);
    free(refseqlist[i].rdna);
    refseqlist[i].id = refseqlist[i].dna = refseqlist[i].rdna = NULL;
  }
  free(refseqlist);
  for (int i = 0; i < maxseqlen+1; ++i) {
    free(SW[i]);
  }
  free(SW);
  free_comp_matrix(comp_Matrix);
  refseqlist    =  NULL;
  queryseq.id   =  NULL;
  queryseq.dna  =  NULL;
  SW            =  NULL;
  comp_Matrix   =  NULL;

  MPI_Finalize();
  return (EXIT_SUCCESS);
}

