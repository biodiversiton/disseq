#
# Disseq package for calculating distances between reads in NGS samples.
#
# Copyright 2022, INRAE
# Authors: Alain Franc and Jean-Marc Frigerio.
#
#===========================================================================
# MPIdisedi executable
#===========================================================================

 add_executable(mpidisedi mpidisedi.c func.c)
 target_link_libraries(mpidisedi common MPI::MPI_C)
 install(TARGETS mpidisedi DESTINATION bin)
