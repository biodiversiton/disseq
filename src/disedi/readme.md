# Compilation de disseq depuis src:

<code>rm -rf Build && mkdir Build && cd Build && cmake -D DISSEQ_SEQ=ON .. && cd disseq && make 2> LOG; ll LOG</code>
# Launch
  ./disseq -ref ../../test_files/h5Query_1.fas && more outfile.
  
should print:
````
computing : 
nb operation to do : 1
1 done
#format_sw	algo:smith-waterman	nb_ref_seq:2	longer_ref_seq:177
###Ref seq File Name : ../../test_files/h5Query_1.fas
###matrix return for each pair of sequence the distance computed as ((shorter sequence le
ngth - SW score) / 2.0)
SEQ_ID	Q2_60	Q2_120
Q2_60	0.00	0.00
Q2_120	0.00	0.00
````

# Compilation de disseq_api depuis src:

<code>rm -rf Build && mkdir Build && cd Build && cmake -D DISSEQ_API=ON .. && cd disseq && make 2> LOG; ll LOG</code>

# Launch
  ./disseq_api

should print:
````
0: 37.00	35.00	29.00	32.00	7.50	
1: 31.50	33.50	38.00	37.00	29.50	
2: 40.50	40.00	44.00	6.50	29.50	
3: 44.50	37.00	45.00	26.00	38.50	
4: 37.00	39.00	30.50	43.00	25.00	
5: 32.00	35.50	24.00	33.00	4.50	
6: 37.00	39.50	43.50	6.50	30.00	
7: 36.00	35.50	25.00	30.00	1.50	
8: 40.00	39.00	43.50	5.50	30.50	
9: 37.50	37.00	43.00	3.50	30.50
````