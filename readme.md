# Project Disseq 

This project consists in providing tools for computing distances, either as edit distances, or based on kmer histograms, between all pairs of reads within a sample, or between two samples. This is a first step for supervised or unsupervised clustering of reads in an environmental sample to build OTUs or to map them on a reference database. It makes available two methods for computing distances, hence two programs, with three output formats for each program (see below).   


The two methods for computing distances are:
<ul>
<li> edit distances with Smith-Waterman and/or Needleman-Wunsch algorithm </li>
<li> L1 distance between kmer histograms.
 </ul> 

 The programs are named as follows:


|program | method for distance |
|-----------------|:-------------|
| **disedi**| edit distance | 
| **diskm**| kmers distance |


All the versions of these programs are provided as parallelised with MPI, which is included as a dependency while loading the package. Indeed, as computations for different pairs *(i,j)* are independent, both programs provide a distributed version with a map-reduce process, implemented with MPI. This enables to derive computations for large environmental samples (up to 100,000 reads for `disedit` and up to 10,000 reads for `diskm`).    


 ## input/output files   

 For both programs,    

<ul>
<li> The input file is a `fasta`  file </li>
<li> The output file is an array of pairwise distances with three formats described below.</li>
</ul>

If the data are available in `fastaq` format, a small utilitary program is given for translating it into a `fasta` file, with an argument to select the minimum quality.




## output formats

Three output formats are available for each program, `disedit` or `diskm`, selected with an option. 

<ul>
<li> a full matrix as an ascii file, with seqids as rownames and column names, and tabs as delimiters on each row </li>
<li> a `hdf5` structure, with three datasets given below </li>
<li> a sparse three columns format, detailed below.
</ul>


The format is selected with the option

```
-f, --format
```

with values 

| value | format |
|-----------------|:-------------|
| **full**| full matrix, ascii format | 
| **h5**| full matrix, hdf5 format |
| **sparse** | sparse format |


#### hdf5 format

The datasets in the hdf5 format are:
 
|dataset | short description |
|-----------------|:-------------
| **distances**| the distance array, same format as the ascii format | 
| **seqid**| list of sequence identifiers |
| **word**| list of sequences / reads |

#### sparse format

When the matrix is large, and especially when distances are computed between a set of queries and a set of references (or subjects in blast world), it is more efficient to work with a three columns sparse format. It is inspired by a classical format for sparse matrices, i.e. large matrices that have zeros almost everywhere and non-zero values in a small number of cells identified by their row and column numbers. For each such pair, a line `i,j,value` is written. Here, it is adapted by selecting  `i,j,value` whenever the value is below a selected threshhold, and, instead of indices `i, j`, rows and columns with such values  are given as query seqid and reference seqid.

The format is presented here in more details (a line for each pair query x reference with distance smaller than the selected threshold):

|column | format | what it is |
|-----------------|:-------------|:--------------|
| first column| string | query seqid |
| second column | string | reference seqid |
| third column | float | distance query x ref|


# Install

There are two ways for installing `disseq`: 
<ul> 
<li> cloning the version of this gitlab (for developpers preferably) </li> 
<li> installing a debian package (for users) </li>
</ul>

The procedure for installing a debian package from the gitlab is given here, and the procedure for cloning the gitlab and having access to the source codes, compiling them, for developers preferably, is given at the end of this `readme`. 



# Installation as a debian package (provisional) 


Installing as a debian package frees the user from any worries about dependencies, such as MPI, since they are embedded in the package, except for the following external dependencies:

* `jellyfish`, installed by

```
sudo apt install jellyfish
```

If jellyfish is installed, go to the gitlab interface, and 
<ul>
<li> on left hand menu, select `Deploy`  </li>
<li> in Deploy, go to item `Package Registry` </li>
<li> click the item `disseq` tagged with `debian11 Generic` </li>
<li> select the the most recent file; clicking on it will download it in `Downloads` directory (or `Téléchargements`) </li>
</ul>

```
download the `.deb`  file

```


Go into the directory where it has been downloaded, and type

```
sudo dpkg -i
```

The binary file will be available in user directory (to be confirmed)

```
/usr/local/bin
``` 

and the commands 

```
disedi, diskm
```

available from any directory, like 

```
alain@bapi-podeple126:~$ disedi
usage: disedi [-h] -q QUERYFILE -o OUTFILE [-r REFFILE] [-a {sw,nw}]
              [-f {full,sparse,h5}] [--np NP] [-d DISMAX]

options:
  -h, --help            show this help message and exit
  -q QUERYFILE, --query QUERYFILE
                        fasta file to be studied
  -o OUTFILE, --out OUTFILE
                        outfile name
  -r REFFILE, --ref REFFILE
                        reference fasta file, for supervised analysis
  -a {sw,nw}, --algo {sw,nw}
                        edit distance, can be sw or nw (default sw)
  -f {full,sparse,h5}, --format {full,sparse,h5}
                        output format, can be h5, full or sparse (default
                        full)
  --np NP               number of cores to use (default 7)
  -d DISMAX, --dismax DISMAX
                        max value of distance to be printed in sparse format
                        (default 30))
alain@bapi-podeple126:~$ 

```

# Program `disedi`

```
usage: disedi [-h] -q QUERYFILE -o OUTFILE [-r REFFILE] [-a {sw,nw}]
              [-f {full,sparse,h5}] [--np NP] [-d DISMAX]


```

Help can be obtained by typing 
```
disedi -h      or      disedi
``` 

### What it does

A fasta file being given (named _query_), computes pairwise edit distances  
<ul>
<li> for all pairs of queries, for unsupervised clusering, if no reference file is given </li>
<li> for all pairs _query_ x _references_ if a reference fasta file is given as well (option -r). </li>
</ul>

Working with a query file only is often the starting point for unsupervised clustering, whereas working with a query file and a reference file is often the starting point for supervised clustering.


### Arguments 

The arguments of `disedi` are given in the following table. Bold value is default value.

|argument | short | what it is | values |
|-----------------|:----------|:-------------|:---------------:|
| **--query**| **-q** | query file | name of a fasta file|
| **--ref** | **-r** |  reference file | name of a fasta file (optional) |
| **--out**| **-o** | distance file | name of the distance file|
| **--algo** | **-a** | algorithm for alignment score | **sw**,  *nw*|
| **--format**   | **-f** | output format   | **full**, h5, sparse   |
| **--nprocs** | **-np** | number of MPI processes | (default is 7) |
| **--dismax** | **-d** | maximum distance to be printed in sparse format | (default is 30) |  



They are briefly explained here:
* **query** is a fasta file on which pairwise edit distances will be computed . If no reference is given, disedi computes all pairwise distances between sequences in query file. If a reference file is given, it computes all pairwise distances between sequences in query and sequences in ref.

* **ref**: see **query**

* **out:** the distance file as outcome. 

* **format** the format through which the distance matrix is returned. Three formats are available (see above for details):  **full** : a full ascii matrix, with seqids as rownames and colnames, and values on a row delimited by tabs `\t` ; **h5** a matrix in hdf5 format, recommended for large data sets (over 10,000 sequences) ; **sparse**: distances smaller than a give threshold ony are written; the threshold is given by **-d** or **-dismax**.


* **algo** algorithm to be used for computing edit distances. Two are available: `sw` for Smith-Waterman, the default value, and `nw` for Needleman Wunsch (recommended if sequences lengths are very similar) 

* **nprocs** number of processes running supervised by MPI. Default value is 7 because (urrently), most of laptop have 8 cores, and 7 uses them all, but one for other tasks on the machine.

* **dispax** (see **format**): maximum value for a distance for it to be printed in sparse format.


# Program `diskm`


## What it does

A fasta file being given (named _query_), computes kmer distances  
<ul>
<li> for all pairs of queries, for unsupervised clusering, if no reference file is given </li>
<li> for all pairs query x references if a reference fasta file is given as well (option -r). </li>
</ul>

```
usage: diskm [-h] -q QUERYFILE -o OUTFILE [-r REFFILE] -k K [-f {full,sparse,h5}] [-d DISMAX] [-np NPROCS]

```

Help can be obtained by typing 
```
diskm -h      or      diskm
``` 

## How it works

`diskm` is a python program for computing distances between sequences or reads from their histogram of kmers. A kmer is a word of length *k* on the alphabet (A,C,G,T). There are \$4^k\$  possible words, and the histogram is the counting of how many times each word appears in the sequence. `jellyfish`, called by `diskm`, is a very efficient program to build those histograms. If *k* is large (say, > 12), all words are not represented. `jellyfish` keeps the counts of those words which are represented only, which is often a vey small fraction of the possible ones. In a second step, the distance between two sequences or reads is computed as the distance between the histograms, with L1 norm. This second step is distributed between cores with MPI. It can handle large kmers (like *k = 24* for example), but not very large number of reads or sequences (up to, say, 5,000).


## Arguments 

The arguments of `diskm` are given in the following table. Bold value is default value. Most of them are common with `disedi`. The only specific one is `k`, the length of the kmer. Short explanation is given here for all arguments, most of them repeated from `disedi`.   

|argument | short | what it is | values |
|-----------------|:----------|:-------------|:---------------:|
| **--query**| **-q** | query file | name of a fasta file|
| **--ref** | **-r** |  reference file | name of a fasta file (optional) |
| **--out**| **-o** | distance file | name of the distance file|
| **--k** | **-k** | lentgth of the kmers | an integer (default is 6) |
| **--format**   | **-f** | output format   | **full**, h5, sparse   |
| **--nprocs** | **-np** | number of MPI processes | (default is 7) |
| **--dismax** | **-d** | maximum distance to be printed in sparse format | (default is 30) |  



They are briefly explained here:
* **query** is a fasta file on which pairwise edit distances will be computed . If no reference is given, disedi computes all pairwise distances between sequences in query file. If a reference file is given, it computes all pairwise distances between sequences in query and sequences in ref.

* **ref**: see **query**

* **out:** the distance file as outcome. 

* **format** the format through which the distance matrix is returned. Three formats are available (see above for details):  **full** : a full ascii matrix, with seqids as rownames and colnames, and values on a row delimited by tabs `\t` ; **h5** a matrix in hdf5 format, recommended for large data sets (over 10,000 sequences) ; **sparse**: distances smaller than a give threshold ony are written; the threshold is given by **-d** or **-dismax**.


* **k** The length of the kmers. Short and long kmers are accepted (see section *how it works*). Default value is *k=6* (hexamers), with *4^6 = 4,096* possible kmers.

* **nprocs** number of processes running supervised by MPI. Default value is 7 because (urrently), most of laptop have 8 cores, and 7 uses them all, but one for other tasks on the machine.

* **dispax** (see **format**): maximum value for a distance for it to be printed in sparse format.



# Tutorial with available Datasets 

Some datasets are available with `Disseq` and presented here in a short tutorial. It is advised to work in the directory where the data are located, selected by the user. hence, the first step is to import the available datasets into this directory. 


## Short datasets, mainly for barcoding


### Available datasets

The datasets are:

* Pinus_matK.fas : 95 sequences of Pinus species from litterature

* atlas_guyane.fas : 1498 sequences of trees in French Guiana, presented and avaibale too at adress https://doi:10.15454/XSJ079

* atlas_guyane_laurales : 103 sequences from atlas_guyane.fas of the prder of Laurales (a difficult family for both botany and barcoding)

The datasets avaibale with `Disseq` package are located in directory 

```
/usr/local/share/disseq
```

when installing the package. This directory is root, and, unless the user accesses to it with a `sudo` command, which is not recommended, files in it cannot be modified nor erased. They can be however copied elsewhere, where they can be modified. The tutorial continues with file `atlas_guyane_laurales.fas`. 

### How to import them

Let us assume the user wishes to work in a directory named `datasets/laurales/`. To import avaibale datasets, e.g.  `atlas_guyane_laurales.fas`,  

* go into this directory
* import the selected dataset by typing
``` 
[...]/datasets/laurales$ cp /usr/local/share/disseq/atlas_guyane_laurales.fas .
``` 

(do not forget the dot ...). Then, file  `atlas_guyane_laurales.fas` has been copied, as seen with
```
[...]/datasets/laurales$ ls -l
total 40
-rw-r--r-- 1 alain alain 39547 août   2 13:36 atlas_guyane_laurales.fas
```

For computing the pairwise distance matrix with Smith-Waterman algorithm, and call `atlas_guyane_laurales.sw.dis` the distance matrix, just type 
```
$ disedi -q atlas_guyane_laurales.fas -o atlas_guyane_laurales.sw.dis 
``` 

To check that the distance file has been computed and written, type

```
[...]/datasets/laurales$ ls -l
total 148
-rw-r--r-- 1 alain alain 39547 août   2 13:36 atlas_guyane_laurales.fas
-rw-rw-r-- 1 alain alain 60779 août   2 13:43 atlas_guyane_laurales.sw.dis
alain@bapi-podeple126:~/_AF/datasets/laurales$ 
```

and the file (ascii file, delimiters on rows by tabs) should look like (to be showed)


## Large datasets, mainly for metabarcoding
In this case, datastes for this tutorial are publicly available on repository `datagouv` accessible at `https://www.data.gouv.fr/fr/`.

### How to import datasets

Large datasets (fasta files in hdf5 format)

```
wget (retrieve a fasta file from dataverse)
``` 

# What else?

Why and how to compute pairwise distances? We cannot propose a survey of such a wide area. A clear introduction to the topic in Nai and Zhang (2005), or Yang (2006).



We simply mention that, with a pairwise distance matrix, be it edit distance or kmers distance, one can:

* build a low dimensional point cloud where each point is a read/sequence, and such that the euclidean distance in the cloud is as close as possible to the evolutionary distance. Two approaches are standard: Multidimensional Scaling, either classical one or least square scaling, and t-SNE.

* build a dendrogram with hierarchical aggregative clustering, and analyse whether the clusters can be considered as OTUs (unsupervised clustering)

* build a barcoding graph: select a threshold $t$, and build a graph for which one read/sequence is one node, and there is an edge between two vertices if the distance between the reads/sequences is lower than the threshold. This is historical approach to OTU, where two reads belong to the same OTU if their distance is lower than the gap. Such a relation is not transitive, and cannot yield in general equivalent classes, but connected components of such a graph are exactly the clusters of a HAC with single linkage (with curring the dendrgram at a height depending on $t$).

For these possible developments, see `yapotu` project at https://gitlab.inria.fr/biodiversiton/yap.


# Installation by cloning the gitlab version

Here is a detailed stepwise procedure to install `disseq` from this gitlab (Unix) 

Install `mpi` and `cmake` in case these libraries have not been installed beforehand (can be done from any directory);   

```
sudo apt install openmpi-bin   
sudo apt install cmake  
```

 

clone the git where you want it to be installed (in $)  
(This will create a new subdirectory `disseq` with all required files  

```
$ git clone git@gitlab.inria.fr:biodiversiton/disseq.git

```

go into disseq   

```
cd src
```

not useful for first installation   

```
rm -rf Build
```

required for first intallation  

```
mkdir Build  
cd Build
```

to compile the sources  

```
cmake ..  
make   
sudo make install   
```

The executables are in directory /usr/local/bin: disseq and mpidisseq   
they can be launched from anywhere. This will display the help for each  

```
disseq -h   
mpidisseq -h 
```  


# References

## References for the methods 

#### Evolutoinary distances

Nei, M. and Zhang, J. (2005). Evolutuionary Distances: Estimation. Encyclopedia of Lofe Sciences. John Wiley & Sons.

Yang, Z. (2006). Computational Molecular Evolution. Oxford Series in Ecology and Evolution, Oxford University Press.

#### Edit distances

Gusfield, D. (1997) Algorithms on strings, trees and sequences. Cambridge University Press.

Needleman, S. B., & Wunsch, C. D. (1970). A general method applicable to search for similarities in the amino-­acid sequence of two proteins. *Journal of Molecular Biology*, **48,** 443-­453.

Smith, P. D., & Waterman, M. S. (1981). Identification of common molecular subsequences. *Journal of Molecular Biology*, **147,** 195-­197.

#### jellyfish

Marcais; G. & Kingsford, K. (2011). A fast, lock-free approach for efficient parallel counting of occurrences of k-mers. *Bioinformatics* **27(6):** 764-770 (first published online January 7, 2011) doi:10.1093/bioinformatics/btr011


## References for the datasets


### Barcoding (Guiana trees)

Abouabdallah, M. A., Peyrard, N. and Franc, A., 2022. Does clustering of DNA barcodes agree with botanical classification directly at high taxonomic levels? Trees in French Guiana as a case study. *Molecular Ecology Resources*, **22(5):** 1-16, 
https://doi.org/10.1111/1755-0998.13579

Caron, H, Molino, J‐F, Sabatier, D, et al., 2019, Chloroplast DNA variation in a hyperdiverse tropical tree community. *Ecol Evol.* **9:** 4897-4905. 
https://doi.org/10.1002/ece3.5096 

### Metabarcoding (diatoms in Arcachon Bay))

Auby I, Méteigner C, Rumebe M, Chancerel E, Salin F, Aluome C, Barraquand F, Carassou L, Del Amo Y, Meleder V, Petit A, Picoche C, Frigerio JM, Franc A (2022) Malabar data-sets used in study “OTU quality from dissimilarity arrays”. Recherche Data Gouv, V1. doi: https://doi.org/10.57745/7T2UCB


Cros, M.-J., Frigerio, J.-M.,  Peyrard, N. & Franc, A., 2024. Simple approaches for evaluation of OTU quality based on dissimilarity arrays. *MBMG*, **8** 25-44,  https://doi.org/10.3897/mbmg.8.108649



# ID

**Contributors**

* Philippe Chaumeil
* Alain Franc
* Jean-Marc Frigerio
* Florent Pruvost
* David Sherman
* Sylvie Thérond

**Maintainer:** Jean-Marc Frigerio

**Contact:** <Jean-Marc.Frigerio@inrae.fr>

**started:** April 11th, 2018
**version:** 24.08.01




